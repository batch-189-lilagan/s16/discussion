/*
    You can declare functions by using:
    1. Function keyword
    2. Function name
    3. open/close parenthesis
    5. open/close curly braces
*/
function sayHello() {
    console.log('Hello there!')
};
// Invoking the function... 
sayHello();


// You can assign a function to a variable
let sayGoodBye = function() {
    console.log('Good Bye!');
};
sayGoodBye();


// Re-assigning function value to an existing variable function
sayGoodBye = function() {
    console.log('Au Revior!');
};
sayGoodBye();


// You can assign a function to a const variable
const sayHelloInJapanese = function() {
    console.log('Ohayo');
};
sayHelloInJapanese();


// Arrow Function 💡
let sayName = () => console.log('Charles');
sayName();


// Self Invoking Function 💡
(function() {
    console.log('self-invoking ');
})();


// Global and Local Scope
let action = 'run';

function doSomethingRandom() {
    console.log(action);
};
doSomethingRandom();


// Nested Functions
function viewProduct() {

    console.log('Viewing a product');

    function addToCart() {
        console.log('Added product to cart');
    };
    addToCart();
};
viewProduct();


// Alert window
function singASong() {
    alert('La la la 🎶');
};
singASong();
// Will run after the windows has been closed
console.log('Clap clap clap 👏🏻');


// Prompt Window
function enterUserName() {
    let userName = prompt('Enter your username');
    console.log('Welcome ' + userName);
}
enterUserName();


// You can use a prompt outside of a function
let age = prompt('Enter your age');
console.log(age);